package handler

import (
	"TodoApp-Server/model"
	"TodoApp-Server/service"
	"github.com/golang/mock/gomock"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"github.com/zemirco/couchdb"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func Test_ShouldReturnAllTodos(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	expectedAsString := `[{"_id":"1234","_rev":"9876","content":"test todo","completed":false}]`

	var todoList []model.TodoModel
	todoList = append(todoList, model.TodoModel{
		Content:   "test todo",
		Completed: false,
		Document:  couchdb.Document{ID: "1234", Rev: "9876"},
	})
	services := service.NewMockService(controller)
	services.EXPECT().GetTodos().Return(todoList).Times(1)
	handler := TodoHandler{services}
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/alltodo", nil)
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)

	if assert.NoError(t, handler.GetTodos(ctx)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, expectedAsString+"\n", rec.Body.String())
	}
}

func Test_ShouldCreateTodo(t *testing.T) {

	controller := gomock.NewController(t)
	defer controller.Finish()

	request := model.TodoModel{Content: "test todo"}
	requestAndExpectedAsString := `{"content":"test todo","completed":false}
`

	services := service.NewMockService(controller)
	services.EXPECT().CreateTodo(gomock.Eq(request)).Return(request).Times(1)
	handler := TodoHandler{services}
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/addtodo", strings.NewReader(requestAndExpectedAsString))
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)
	if assert.NoError(t, handler.CreateTodo(ctx)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
		assert.Equal(t, requestAndExpectedAsString, rec.Body.String())
	}
}
