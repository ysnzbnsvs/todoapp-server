package config

import (
	"github.com/joho/godotenv"
	"github.com/zemirco/couchdb"
	"net/url"
	"os"
	"strings"
)

type CouchDBConfiguration struct {
	Client   *couchdb.Client
	Database couchdb.DatabaseService
}

func (cc CouchDBConfiguration) Init() CouchDBConfiguration {
	if isInTests() {
		godotenv.Load("../.env")
	}
	u, _ := url.Parse(os.Getenv("COUCHDB_URL"))
	cc.Client, _ = couchdb.NewAuthClient(os.Getenv("COUCHDB_USER"), os.Getenv("COUCHDB_PASSWORD"), u)

	cc.Client.Create(os.Getenv("COUCHDB_NAME"))

	cc.Database = cc.Client.Use(os.Getenv("COUCHDB_NAME"))

	return CouchDBConfiguration{
		Client:   cc.Client,
		Database: cc.Database,
	}
}

func getCouchDBClient() (*couchdb.Client, error) {
	u, _ := url.Parse(os.Getenv("COUCHDB_URL"))
	c, err := couchdb.NewAuthClient(os.Getenv("COUCHDB_USER"), os.Getenv("COUCHDB_PASSWORD"), u)
	return c, err
}
func getCouchDBDatabase(client *couchdb.Client) {
	client.Create(os.Getenv("COUCHDB_NAME"))
}
func useCouchDBDataBase(client *couchdb.Client) couchdb.DatabaseService {
	return client.Use("COUCHDB_NAME")
}
func isInTests() bool {
	for _, arg := range os.Args {
		if strings.Contains(arg, "test") {
			return true
		}
	}
	return false
}
