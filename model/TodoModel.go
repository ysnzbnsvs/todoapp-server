package model

import "github.com/zemirco/couchdb"

type TodoModel struct {
	couchdb.Document
	Content   string `json:"content"`
	Completed bool   `json:"completed"`
}
type TodoModels struct {
	couchdb.Document
	Id string `json:"id"`
}
type TodoResponseModel struct {
	_id       string `json:"id"`
	rev       string `json:"rev"`
	Content   string `json:"content"`
	Completed bool   `json:"completed"`
}
